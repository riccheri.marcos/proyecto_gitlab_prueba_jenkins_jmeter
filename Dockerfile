FROM node:8
MAINTAINER merv@merv
WORKDIR /prueba
COPY . .
RUN apt-get -y update
RUN apt-get -y install wget
RUN wget https://www.python.org/ftp/python/3.9.9/Python-3.9.9.tgz
RUN tar -xzvf Python-3.9.9.tgz
RUN apt-get install -y default-jre-headless 
RUN apt-get install -y python3-tk
RUN apt-get install -y python3-pip
RUN apt-get install -y python3-dev
RUN apt-get install -y libxml2-dev libxslt-dev zlib1g-dev net-tools
RUN python3 -m pip install --upgrade wheel setuptools Cython
RUN python3 -m pip install bzt==1.13.0
#RUN wget 'https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.4.3.tgz'
